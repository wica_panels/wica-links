#!/bin/bash

# Script starts here...
# Exit immediately if anything unexpected happens.
set -e
#set -x

# Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'.
function dist_create() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     git worktree add -f "$1" "$1"
  else
     echo "Sets up a local worktree named 'dist_xxx' to track the remote branch named 'dist_xxx'."
  fi
}

# Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'.
function dist_pull() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git pull
  else
     echo "Pulls the latest information from the remote branch named 'dist_xxx' to update the local worktree named 'dist_xxx'."
  fi
}

# Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'.
function dist_commit() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git add .
     git diff-index --quiet HEAD || git commit -m "updates" .
     cd ..
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' to the local branch named 'dist_xxx'."
  fi
}

# Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'.
function dist_push() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     cd "$1"
     git push
     cd ..
  else
     echo "Pushes the latest information from the local worktree named 'dist_xxx' to update the remote branch named 'dist_xxx'."
  fi
}

# Trigger autodeployment on the autodeployment server associated with the supplied dist area."
function dist_deploy() {
  if [[ "$1" == "dist_dev" ]]; then
     curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica-dev.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/wica-links.git&relTargetPathArg=gfa-wica-panels/repos/wica-links"
  elif [[ "$1" == "dist_prod" ]]; then
     curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://gfa-wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/wica-links.git&relTargetPathArg=gfa-wica-panels/repos/wica-links"
  elif [[ "$1" == "dist_ext" ]]; then
     curl -H "Content-Type: application/x-www-form-urlencoded" -X POST "https://wica.psi.ch:8443/deploy?gitUrl=git@gitlab.psi.ch:wica_panels/wica-links.git&relTargetPathArg=gfa-wica-panels/repos/wica-links"
  else
     echo "Trigger autodeployment on the autodeployment server(s) associated with the supplied dist area."
  fi
}

# Commits the current state of the local worktree named 'dist_xxx' and pushes it to the remote branch named 'dist_xxx'.
function dist_publish() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     dist_commit "$1"
     dist_push "$1"
     dist_deploy "$1"
  else
     echo "Commits the current state of the local worktree named 'dist_xxx' and pushes it to the remote branch named 'dist_xxx'."
  fi
}

# Deletes and removes the local worktree and branch named 'dist_xxx'.
function dist_clean() {
  if [[ "$1" == "dist_dev" || "$1" == "dist_prod" || "$1" == "dist_ext" ]]; then
     rm -fr "$1"
     git worktree remove "$1"
     git branch -D "$1"
  else
     echo "Deletes and removes the local worktree and branch named 'dist_xxx'."
  fi
}

#  Creates the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext').
function dist_create_all() {
  dist_create "dist_dev"
  dist_create "dist_prod"
  dist_create "dist_ext"
}

# Publishes the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext') to their corresponding remote branches.
function dist_publish_all() {
  cp -a src/dev/index.html dist_dev/index.html
  cp -a src/prod/index.html dist_prod/index.html
  cp -a src/ext/index.html dist_ext/index.html
  cp -a src/psi-site.jpg dist_dev/background.jpg
  cp -a src/psi-site.jpg dist_prod/background.jpg
  cp -a src/psi-site.jpg dist_ext/background.jpg

  dist_publish "dist_dev"
  dist_publish "dist_prod"
  dist_publish "dist_ext"
}

#  Deletes and removed the local worktrees ('dist_dev', 'dist_prod' and 'dist_ext').
function dist_clean_all() {
  dist_clean "dist_dev"
  dist_clean "dist_prod"
  dist_clean "dist_ext"
}
