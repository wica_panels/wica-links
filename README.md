# Overview

This is the **wica-links** git repository which provides a system for linking
from the Wica landing pages on various wica servers to the relevant wica displays
(which are maintained and deployed in separate Git repos).

# Supported Servers

The following autodeploy servers will be AUTOMATICALLY triggered by publications from this project when the 
project is installed as generally anticipated, ie on a machine running inside the PSI Office Network:

* [GFA Wica External Server](https://wica.psi.ch:8443)
* [GFA Wica Production Server](https://gfa-wica.psi.ch:8443)
* [GFA Wica Development Server](https://gfa-wica-dev.psi.ch:8443)

# How to deploy

Edit the **'dev/index.html'**, **'ext/index.html'** and **'prod/index.html'**
files in the src directory as required. When ready for deployment on the
associated servers use the command below:
```
  npm run dist_publish_all
```

This will push these directories to the respective branches on the GitLab
Server, and send a command to the relevant autodeploy servers to download
and install the required branch to the local filesystems.

# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.
