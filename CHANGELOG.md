# Overview

This log describes the functionality of tagged versions within the repository.

# Tags  
* [1.0.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.0.0)
  Initial release.
  
* [1.1.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.1.0)
  Updated to support PROSCAN classic and responsive designs.

* [1.2.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.2.0)
  Added PSI background image.

* [1.2.1](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.2.1)
  Retired PROSCAN Responsive link.

* [1.2.2](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.2.2)
  * DEV_LINKS: Now links to Machine Protection System main page.
  * DEV_LINKS: Removed PROSCAN Classic.
  * EXT_LINKS: Removed unused definition.
  * EXT_LINKS: Removed PROSCAN Classic.
  * PROD_LINKS: Removed PROSCAN Classic.
  * PROD_LINKS: Now links to Machine Protection System main page.
  * Updated CHANGELOG with details of release 1.2.2.

* [1.3.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.3.0)
  * ILK_LINKS: add new branch to handle links file on ILK production servers.
  * Updated CHANGELOG with details of release 1.3.0.

* [1.4.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.4.0)
  * ILK_LINKS: content is now determined dynamically by hostname.

* [1.4.1](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.4.1)
  * Reorganised the src directory, hopefully for greater clarity.
  * Updated the README file witha dditional information.
  * Removed eslint since it was not installed or being used.

* [1.5.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.5.0)
  * Added support for PROSCAN COMET Cryostat Shield Temeperature monitoring.

* [1.6.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.6.0)
  * Added support for HIPA Responsive Display. 
  
* [1.7.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.7.0)
  * Reduced size of PSI background image to ~600 KB.
  * Now supports Web Application Firewall (WAF) Logout. 

* [1.8.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.8.0)
  * WAF Logout Enhancement: now triggers WAF logout using new 'logout.html' endpoint.
  * WAF Logout Enhancement: reduced WAF autologin time to 1.5s following change to WAF logout timer to 1s.

* [1.9.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.9.0)
  * ILK: Add support for ILKCS Server Login. Removed direct link to ILK Software.
  * ILK: Changed title: RPS Assistant Machine Options -> ILK Machine Options.

* [1.10.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.10.0)
  * DEV_LINKS: Added link to Nicole's SF Test Page
  * EXT_LINKS: Added link to Nicole's SF Test Page
  * PROD_LINKS: Added link to Nicole's SF Test Page

* [1.11.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.11.0)
  * DEV_LINKS: Add support for HIPA IW2 vacuum panel.
  * EXT_LINKS: Add support for HIPA IW2 vacuum panel.
  * PROD_LINKS: Add support for HIPA IW2 vacuum panel.

* [1.12.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.12.0)
   * Reorganize external and production index pages for improved consistency.

* [1.13.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.13.0)
  * Tidy up index pages ready for lightning talk.

* [1.14.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.14.0)
  * Fix typo.
  * Add PROSCAN and HIPA interlock viewer to menu options when appropriate.
  * Update Wica DEV menu to look like Wica PROD menu.
  * Add support for automatic deployment to HIPA and PROSCAN interlock viewer machines.
  * Create release 1.14.0

* [1.15.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.15.0)
  * Now links to new unified interlock ui.
  * Create release 1.15.0

* [1.16.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.16.0)
  * Adapt GFA RF Displays to include SLS TS and LAB.
  * Create release 1.16.0

* [1.17.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.17.0)
  * Add support for ISTS.
  * Create release 1.17.0

* [1.18.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.18.0)
  * CHORE: Cleanup landing page.
  * CHORE: Install ilk favicon. Rename 'psi_site.jpg' to 'psi-site.jpg'.
  * ENHANCEMENT: PROSCAN link now goes straight to login page.
  * ENHANCEMENT: ISTS links now goes straight to login page.
  * ENHANCEMENT: HIPA viewer links now goes straight to login page.
  * ENHANCEMENT: Add title to HIPA and PROSCAN ILK viewer pages.
  * ENHANCEMENT: HIPA links now goes straight to login page.
  * CHORE: Update README with latest situation.
  * ENHANCEMENT: GFA ILK DEV and GFA ILK PROD menu now offers direct link to ILKCS login page.
  * ENHANCEMENT: Create release 1.18.0

* [1.19.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.19.0)
  * CHORE: Eliminate support for dist_ilk. In the future ILK deployments will be handled from a separate git repository.
  * CHORE: Create release 1.19.0

* [1.20.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.20.0)
  * ENHANCEMENT: Add support for Thomas Schietinger SF Test Page.
  * CHORE: Minor formatting changes.
  * ENHANCEMENT: Add support for Thomas Schietinger SF Test Page. Retire laser displays.
  * ENHANCEMENT: Create release 1.20.0

* [1.21.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.21.0)
  * ENHANCEMENT: Update LLRF descriptions for SLS and LLRF Low Power Teststand.
  * ENHANCEMENT: Create release 1.21.0

* [1.22.0](https://gitlab.psi.ch/wica_panels/wica-links/tags/1.22.0)
  * ENHANCEMENT: Add links to new SwisssFEL Overview and Multiplot displays.
  * ENHANCEMENT: Create release 1.22.0